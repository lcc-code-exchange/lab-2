﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CorvallisOR.Startup))]
namespace CorvallisOR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
